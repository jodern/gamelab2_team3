﻿using UnityEngine;
using System.Collections;

public class Swing : MonoBehaviour {

	public Transform ball;
	public Color playerColor;
	public bool swing = false;
	Vector3 startRot;
	Vector3 swingRot;
	Transform racket;

	public AudioClip hitSound;

	// Use this for initialization
	void Start () 
	{
		racket = transform.FindChild("Racket");
		startRot = transform.forward;
		swingRot = new Vector3 (0.7f, -0.7f, 0.2f);

		playerColor = new Color (1,0.1f,0.1f,0.75f);
		racket.GetComponent<Renderer>().material.color = playerColor;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(swing)
		{
			Vector3 targetDir = ball.transform.position - racket.position;
			Vector3 rot = Vector3.RotateTowards (racket.forward, targetDir,0.1f,0);
			racket.rotation = Quaternion.LookRotation (rot);
			if(racket.rotation == Quaternion.LookRotation (targetDir))
				swing = false;
		}
		else 
		{
			Vector3 rot = Vector3.RotateTowards (racket.forward, startRot,0.1f,0);
			racket.rotation = Quaternion.LookRotation(rot);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.name == "Ball")
		{
			print ("Hit player");
			swing = true;
			if(hitSound)
				GetComponent<AudioSource>().PlayOneShot(hitSound);
		}
	}
}
