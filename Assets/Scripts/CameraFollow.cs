﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	Vector3 oldPos;
	public Transform target;

	// Use this for initialization
	void Start () 
	{
		oldPos = target.position;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		Vector3 deltaPos = target.position - oldPos;
		oldPos = target.position;

		transform.position += deltaPos;
	}
}
