﻿using UnityEngine;
using System.Collections;


public class Player : MonoBehaviour {

	Transform racket;
	public float depth;
	public bool CanServe;
	public Transform ball;
	Game game;
	bool altCamera;
	bool canSwitch; //Can only switch mode ONCE per frame
	public bool canMove = false;
	public Transform movePlane;
	Transform arena;
	float inwardSpeed = 2.3f;
	float camDist;


	public AudioClip hitSound;

	void Awake()
	{
		depth = transform.position.z;
	}
	// Use this for initialization
	void Start ()
	{ 	
		camDist = Camera.main.transform.position.z - transform.position.z;
		arena = GameObject.Find ("Arena").transform;
		racket = transform.FindChild ("Racket");
		altCamera = false;
		game = Camera.main.GetComponent<Game> ();
		CanServe = true;
		Serve ();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		canSwitch = true;
		if(Input.GetKeyDown(KeyCode.Return) & !CanServe)
		{
			Serve();
		}

		if(canMove)
		{
			//move along the arena's z
			transform.position += arena.TransformPoint( new Vector3(0,0,-Input.GetAxis("Vertical")*Time.deltaTime*inwardSpeed));


			Vector3 mousePos = Input.mousePosition;
			mousePos.z = camDist;
			mousePos = Camera.main.ScreenToWorldPoint (mousePos);
			transform.position = mousePos;
		}
//		if(Input.GetKeyDown("1") && !keyControl.enabled && canSwitch) //Switch to keyboard control
//		{
//			SwitchKeyboardControls();
//		}
//		if(Input.GetKeyDown("1") && !mouseControl.enabled && canSwitch) //Switch to mouse control 
//		{
//			SwitchMouseControls();
//		}
//		if(Input.GetKeyDown("2") && keyControl.enabled && !altCamera && canSwitch)
//		{
//			Camera.main.transform.rotation = Quaternion.Euler(15,180,0);
//			Camera.main.transform.localPosition = new Vector3(0,2,-100);
//			altCamera = true;
//			canSwitch = false;
//		}
//		if(Input.GetKeyDown("2") && keyControl.enabled && altCamera && canSwitch)
//		{
//			SwitchKeyboardControls();
//			altCamera = false;
//			canSwitch = false;
//		}

	
		if(CanServe)
		{
			if(Input.GetMouseButtonDown(0))
			{
				ball.GetComponent<Renderer>().enabled = true;
				ball.GetComponent<Rigidbody>().velocity = (Camera.main.ScreenPointToRay(Input.mousePosition).direction).normalized*BallController.ballspeed;
				ball.GetComponent<Rigidbody> ().useGravity = true;
				gameObject.GetComponent<Collider>().enabled = true;
//				ball.GetComponent<BallController>().AI[0].BallImpactPos(ball.GetComponent<Rigidbody>().velocity,ball.transform.position);
				CanServe = false;
				canMove = true;

			}
		}
	}



	public void Serve()
	{
		canMove = false;
		CanServe = true;
		ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
		ball.GetComponent<Rigidbody> ().useGravity = false;
		ball.position = new Vector3 (Random.Range (-movePlane.GetComponent<Collider>().bounds.extents.x*0.5f, movePlane.GetComponent<Collider>().bounds.extents.x*0.5f), Random.Range (-movePlane.GetComponent<Collider>().bounds.extents.y*0.5f, movePlane.GetComponent<Collider>().bounds.extents.y*0.5f), movePlane.position.z);
		gameObject.GetComponent<Collider>().enabled = false;
	}
}
