﻿using UnityEngine;
using System.Collections;


public class BallWallTrace : MonoBehaviour {

	//Requires 4 childs each with a linerenderer component
	LineRenderer[] line;
	Vector2 mapBounds;
	public Transform arena;

	// Use this for initialization
	void Start () 
	{
		mapBounds = Game.mapBounds;
		line = new LineRenderer[4];
		for(int i = 0; i < 4;i++)
		{
			line[i] = arena.FindChild("Line"+i).gameObject.GetComponent<LineRenderer>();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		line[0].SetPosition(0,arena.TransformPoint(new Vector3(-mapBounds.x,mapBounds.y,transform.localPosition.z)));
		line[0].SetPosition(1,arena.TransformPoint(new Vector3(-mapBounds.x,-mapBounds.y,transform.localPosition.z)));
		line [1].SetPosition (0, arena.TransformPoint(new Vector3 (mapBounds.x, mapBounds.y, transform.localPosition.z)));
		line [1].SetPosition (1, arena.TransformPoint(new Vector3 (mapBounds.x, -mapBounds.y, transform.localPosition.z)));
		line [2].SetPosition (0, arena.TransformPoint(new Vector3 (mapBounds.x, -mapBounds.y, transform.localPosition.z)));
		line [2].SetPosition (1, arena.TransformPoint(new Vector3 (-mapBounds.x, -mapBounds.y, transform.localPosition.z)));
		line [3].SetPosition (0, arena.TransformPoint(new Vector3 (mapBounds.x, mapBounds.y, transform.localPosition.z)));
		line [3].SetPosition (1, arena.TransformPoint(new Vector3 (-mapBounds.x, mapBounds.y, transform.localPosition.z)));
	}
}
