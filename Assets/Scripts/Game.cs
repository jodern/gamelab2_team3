﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]

public class Game : MonoBehaviour {

	public static Vector3 mapBounds; // X/Y axis
	Player[] players;
	public Transform[] mapWalls;
	public Transform[] serveNodes;
	Transform arena;
	public static int player2score;
	public static int player1score;

	void Awake()
	{
//		mapBounds = new Vector3 (5,4,12);
//		mapWalls [0].position = new Vector3 (0, -mapBounds.y, 0);
//		mapWalls [0].localScale = new Vector3 (0.2f*mapBounds.x,1,0.2f*mapBounds.z);	//Floor
//		mapWalls [1].position = new Vector3 (mapBounds.x, 0, 0);
//		mapWalls [1].localScale = new Vector3 (0.2f*mapBounds.y,1,0.2f*mapBounds.z);	//Left Wall
//		mapWalls [2].position = new Vector3 (-mapBounds.x, 0, 0);
//		mapWalls [2].localScale = new Vector3 (0.2f*mapBounds.y,1,0.2f*mapBounds.z);	//Right Wall
//		mapWalls [3].position = new Vector3 (0, 0, -mapBounds.z);
//		mapWalls [3].localScale = new Vector3 (0.2f*mapBounds.x,1,0.2f*mapBounds.y);	//Front Wall
//		mapWalls [4].position = new Vector3 (0, 0, mapBounds.z);
//		mapWalls [4].localScale = new Vector3 (0.2f*mapBounds.x,1,0.2f*mapBounds.y);	//Back Wall
//		mapWalls [5].position = new Vector3 (0, mapBounds.y, 0);
//		mapWalls [5].localScale = new Vector3 (0.2f*mapBounds.x,1,0.2f*mapBounds.z);	//Roof
		arena = transform.parent;
	}

	// Use this for initialization
	void Start () 
	{
		player1score = 0;
		player2score = 0;
		players = new Player[GameObject.FindGameObjectsWithTag ("Player").Length];
		for(int i = 0; i < GameObject.FindGameObjectsWithTag("Player").Length; i++)
		{
			players[i] = GameObject.FindGameObjectsWithTag("Player")[i].GetComponent<Player>();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Application.LoadLevel("Main Menu");
		}
	}


	void OnGUI()
	{
		//GUI.Label (new Rect (5, Screen.height - 100, 250, 50), "Press 1 to toggle control mode.\nPress 2 to toggle camera mode");
	}
}
