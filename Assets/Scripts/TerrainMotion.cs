﻿using UnityEngine;
using System.Collections;

public class TerrainMotion : MonoBehaviour {

	public GameObject[] railNodes;
	public Transform activeTrainCart;
	Vector3 terrainOffset;
	public int targetIndex = 0;

	// Use this for initialization
	void Start () 
	{
		terrainOffset = transform.position;

		railNodes = GameObject.FindGameObjectsWithTag ("RailNode");
		for (int i = 0; i < railNodes.Length; i++)
		{
			int num = int.Parse(railNodes[i].name.Substring(8)) - 1;
			GameObject temp = railNodes[num];
			railNodes[num] = railNodes[i];
			railNodes[i] = temp;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.MoveTowards(transform.position,railNodes[targetIndex].transform.position,10*Time.deltaTime);
		

//		if(targetIndex == 0)
//			transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (railNodes [targetIndex].transform.position - railNodes[railNodes.Length-1].transform.position), 5 * Time.deltaTime);
//		else
//			transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (railNodes [targetIndex].transform.position - railNodes[targetIndex - 1].transform.position), 5 * Time.deltaTime);
		if (transform.position == railNodes [targetIndex].transform.position)
			targetIndex++;
		if (targetIndex >= railNodes.Length)
			targetIndex = 0;

	}
}
