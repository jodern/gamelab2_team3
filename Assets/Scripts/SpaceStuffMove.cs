﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceStuffMove : MonoBehaviour {

	Transform[] spaceStuff;
	float[] speeds;

	// Use this for initialization
	void Start () 
	{
		List<int> list = new List<int> ();

		spaceStuff = new Transform[transform.childCount];
		int i = 0;
		foreach (Transform child in transform)
		{
			spaceStuff[i] = child;
			i++;
			child.rotation = Quaternion.Euler(Random.Range(0,360),Random.Range(0,360),Random.Range(0,360));
		}
		speeds = new float[spaceStuff.Length];
		for(int j = 0; j < speeds.Length; j++)
		{
			spaceStuff[j].GetComponent<Rigidbody>().AddForce(new Vector3(Random.value-0.5f,Random.value-0.5f,Random.value-0.5f).normalized*Random.Range(2000,20000),ForceMode.Impulse);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate (new Vector3(0,10 * Input.GetAxis ("Horizontal"),0));
	}

	void OnTriggerEnter(Collider col)
	{
		col.transform.localPosition = -col.transform.localPosition*0.95f;
		col.attachedRigidbody.AddForce(new Vector3(Random.value-0.5f,Random.value-0.5f,Random.value-0.5f).normalized*Random.Range(2000,20000),ForceMode.Impulse);
	}
}
