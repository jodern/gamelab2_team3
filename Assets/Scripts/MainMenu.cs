﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	GameObject quitButton;

	// Use this for initialization
	void Start () 
	{
		quitButton = GameObject.Find ("Quit Game");
		if (Application.isWebPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.isEditor)
				quitButton.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartGameClick()
	{
		Application.LoadLevel ("Game");
	}

	public void OptionsClick()
	{
		Application.LoadLevel ("Options");
	}

	public void QuitGameClick()
	{
		Application.Quit ();
	}

	public void StartAltGameClick()
	{
		Application.LoadLevel ("Game2");
	}
}
