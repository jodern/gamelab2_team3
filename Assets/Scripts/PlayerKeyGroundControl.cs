﻿using UnityEngine;
using System.Collections;

public class PlayerKeyGroundControl : MonoBehaviour {

	Player player;
	float speed = 6;

	// Use this for initialization
	void Start () 
	{
		player = gameObject.GetComponent<Player> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!player.CanServe)
			GetComponent<Rigidbody>().velocity = new Vector3(-Input.GetAxis("Horizontal")*speed,0,-Input.GetAxis("Vertical")*speed);

	}
}
