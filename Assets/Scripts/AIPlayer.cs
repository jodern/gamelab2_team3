﻿using UnityEngine;
using System.Collections;

public class AIPlayer : MonoBehaviour {



	public Transform ball;
	public Transform movePlane;
	public Vector3 targetPos;
	float depth;
	float ballSpeed;
	Vector2 mapBounds;

	void Awake()
	{
		depth = transform.position.z;
		BallImpactPos (ball.position, ball.GetComponent<Rigidbody>().velocity);
	}

	// Use this for initialization
	void Start () 
	{
		mapBounds = Game.mapBounds;
		ballSpeed = BallController.ballspeed;
		targetPos = transform.position;
	}
	

	// Update is called once per frame
	void FixedUpdate () 
	{
		transform.localPosition = Vector3.MoveTowards (transform.localPosition, targetPos, 0.08f);
	}


	public void BallImpactPosOBSOLETE(Vector3 pos, Vector3 dir)
	{
		print ("Hit");


		LayerMask ignore = 1 << 8;
		ignore = ~ignore;

		for(int reflections = 0; reflections <= 20; reflections++)
		{
			Vector3 reflectDir = new Vector3(dir.x,0,dir.z);
			Ray reflectRay = new Ray (new Vector3(pos.x,-Game.mapBounds.y+transform.localScale.y*0.5f,pos.z), reflectDir);
			RaycastHit hit;


			if(Physics.Raycast(reflectRay,out hit,20,ignore))
			{
				Debug.DrawRay(reflectRay.origin,reflectRay.direction.normalized*hit.distance,Color.green,3);

				if(hit.transform.name == movePlane.name || hit.transform.name == transform.name)
				{
					Vector3 tempHitPos = hit.point;
					tempHitPos += new Vector3(Random.Range(-transform.localScale.x*0.4f,transform.localScale.x*0.4f),0,0);
					tempHitPos = new Vector3(Mathf.Clamp(tempHitPos.x,-mapBounds.x+transform.localScale.x*0.5f,mapBounds.x-transform.localScale.x*0.5f),-mapBounds.y+transform.localScale.y*0.5f,transform.position.z);
					targetPos = tempHitPos;

					print ("Setting targetPos to " + targetPos);
					return;
				}
				else 
				{
					reflectDir = Vector3.Reflect(reflectDir,hit.normal);
				}
				reflectRay = new Ray(hit.point,reflectDir);
			}
		}
	}



	public void BallImpactPos(Vector3 pos, Vector3 dir)
	{
		if((dir.z > 0) == (depth > 0)) //Checks if the ball is traveling towards the AI
		{

			Ray reflectRay = new Ray (pos, dir);
			Vector3 reflectDir = dir;
			for(int reflections = 0; reflections <= 20; reflections++)
			{
				RaycastHit hit;
				LayerMask ignore = 1 << 8;
				ignore = ~ignore;
				if(Physics.SphereCast(reflectRay,ball.transform.localScale.x*0.5f,out hit,20,ignore))
				{
					Debug.DrawRay(reflectRay.origin,reflectRay.direction.normalized*hit.distance,Color.green,3);
					if(hit.transform.name == movePlane.name || hit.transform.name == transform.name)
					{
						Vector3 tempHitPos = hit.point;
						tempHitPos += new Vector3(Random.Range(-transform.localScale.x*0.4f,transform.localScale.x*0.4f),Random.Range(-transform.localScale.y*0.4f,transform.localScale.y*0.4f),0);
						tempHitPos = new Vector3(Mathf.Clamp(tempHitPos.x,-mapBounds.x+transform.localScale.x*0.5f,mapBounds.x-transform.localScale.x*0.5f),Mathf.Clamp(tempHitPos.y,-mapBounds.y+transform.localScale.y*0.5f,mapBounds.y-transform.localScale.y*0.5f),transform.position.z);
						targetPos = transform.parent.InverseTransformPoint(tempHitPos);
							print ("Setting target pos");
						return;
					}
					else if((hit.transform.CompareTag("Player") || hit.transform.CompareTag("AI")) & hit.transform.name != transform.name)
					{//Special reflection if the ball will hit the other Player after first reflection
						reflectDir = hit.transform.forward - hit.transform.position + hit.point; 
					}
					else 
					{
						reflectDir = Vector3.Reflect(reflectDir,hit.normal);
						reflectDir = new Vector3(reflectDir.x,(hit.point-new Vector3(reflectRay.origin.x,hit.point.y,reflectRay.origin.z)).magnitude*Physics.gravity.y+reflectDir.y,reflectDir.z);
					}
					reflectRay = new Ray(hit.point,reflectDir);
				}
			}
			ball.GetComponent<Rigidbody>().velocity = new Vector3(Random.value-0.5f,Random.value-0.5f,Random.value-0.5f).normalized*ballSpeed;
		}
	}
}
