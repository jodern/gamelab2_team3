﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (Rigidbody))]

public class BallController : MonoBehaviour {

	public Transform arena;
	public static float ballspeed;
	public AIPlayer[] AI;
	Vector3 oldVelocity;
	bool canRiccochet = true;
	public int dir = -1;
	Player player;
	Rigidbody rigidbody;
	public Text player1ScoreText;
	public Text player2ScoreText;

	// Use this for initialization
	void Start () 
	{
		player1ScoreText.text = "Player score is: " + Game.player1score;
		player2ScoreText.text = "AI score is: " + Game.player2score; 
		rigidbody = GetComponent<Rigidbody> ();
		arena = GameObject.Find ("Arena").transform;
		ballspeed = 10.0f;
		player = GameObject.Find ("Player").GetComponent<Player> ();

	}


	// Update is called once per frame
	void FixedUpdate () 
	{
		//Constant local z speed
		if (player.canMove)
		{
			if (arena.InverseTransformPoint(transform.position).z > arena.FindChild ("Player1MovePlane").transform.localPosition.z)
			{
				Game.player2score += 10;
				player2ScoreText.text = "AI score is: " + Game.player2score; 
				dir = -1;
				Application.LoadLevel("Game Over");
			}
			if (arena.InverseTransformPoint(transform.position).z < arena.FindChild ("Player2MovePlane").transform.localPosition.z)
			{
				Game.player1score += 10;
				player1ScoreText.text = "Player score is: " + Game.player1score;
				dir = 1;
			}
			Vector3 localSpeed = arena.InverseTransformDirection(rigidbody.velocity);
			localSpeed.z = 7*dir;
			rigidbody.velocity = arena.TransformDirection(localSpeed);
//			Debug.DrawRay(transform.position,arena.TransformPoint(localSpeed),Color.yellow,Time.deltaTime);
		}

		if (!rigidbody.isKinematic)
		{
			rigidbody.velocity = rigidbody.velocity.normalized * ballspeed;
		}
		oldVelocity = rigidbody.velocity;
	}


	IEnumerator RiccochetDelay()
	{
		canRiccochet = false;
		yield return new WaitForSeconds (0.5f);
		canRiccochet = true;
	}


	void OnCollisionEnter(Collision col)
	{
		if(canRiccochet)
		{
			StartCoroutine (RiccochetDelay());

			Vector3 reflectDir;

			reflectDir = Vector3.Reflect(oldVelocity,col.contacts[0].normal);

			foreach(AIPlayer AIply in AI)
			{
				Debug.DrawRay(transform.position,reflectDir.normalized*10,Color.red,5);
				if(AIply.enabled)
					AIply.BallImpactPos(transform.position,reflectDir);
			}
		}
	}


	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.CompareTag("Player") || col.gameObject.CompareTag("AI"))
		{
			Vector3 reflectDir;
			print ("Hit player");
			if(col.CompareTag("Player"))
			{;
				dir = -1;
				reflectDir = new Vector3(0,0,-1);
			}
			   else
			{
				dir = 1;
				reflectDir = new Vector3(0,0,1); 
			}
			print (GetComponent<Rigidbody>().velocity);
			foreach(AIPlayer AIply in AI)
			{
				Debug.DrawRay(transform.position,reflectDir,Color.red,5);
				if(AIply.enabled)
					AIply.BallImpactPos(transform.position,reflectDir);
			}
		}
	}


	void OnGUI()
	{
		GUI.Label (new Rect (5, Screen.height - 50, 250, 30), "Press Enter to unstuck ball");
	}
}
